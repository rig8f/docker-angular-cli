FROM node:current-alpine

RUN set -eux; \
  apk add --no-cache --virtual bash \
    coreutils \
    git \
    patch \
    tini \
    unzip \
    zip

# install angular-cli as node user
RUN chown -R node:node /usr/local/lib/node_modules \
  && chown -R node:node /usr/local/bin

USER node
RUN npm install -g @angular/cli

# set npm as default package manager for root
USER root
RUN ng config --global cli.packageManager npm

COPY docker-entrypoint.sh /docker-entrypoint.sh
RUN chown -R node:node /docker-entrypoint.sh \
  && chmod +x /docker-entrypoint.sh

WORKDIR /app
ENTRYPOINT ["/docker-entrypoint.sh"]
CMD ["ng"]
