# Angular-CLI

A Docker image based on current Node, with Angular CLI.

Based on [this Dockerfile](https://github.com/Pivotal-Field-Engineering/angular-cli/blob/master/Dockerfile) and Composer's [Dockerfile](https://github.com/composer/docker).
